import React, { useState } from 'react';
import { View, TextInput, Text, StyleSheet, Image, FlatList } from 'react-native';
import { Card, ListItem, Button, Icon } from 'react-native-elements';
import { TouchableNativeFeedback } from 'react-native-gesture-handler';
import Colors from '../../../../colors';
import { CrediteArray } from '../../../../assets/Constant/Feed';


const Credites = ({ navigation }) => {
  const [checked, setChecked] = useState(false);

  function RowCredit({ item }) {
    return (

      <Card >
        <View style={{ flex: 1 }}>
          <View style={{ flex: 1, flexDirection: 'row', justifyContent: 'space-between' }}>
            <Text>{item._id}</Text>
            <Text style={{ color: '#c3c4c4' }}>{item.datentime}</Text>
          </View>
          <View style={{ flexDirection: 'row', margin: 10, alignContent: 'center' }}>
            <Image source={item.image} style={{ height: 40, width: 40, borderRadius: 20 }} />
            <View style={{ marginLeft: 10 }}>
              <Text style={{}}>{item.title}</Text>
              <Text style={{}}>Cost: {item.cost}</Text>
            </View>
          </View>
          <View style={{ marginTop: 10 }}>
            <View style={{ justifyContent: 'space-between', flex: 1, flexDirection: 'row' }}>
              <Text style={{}}>Balance</Text>
              <Text style={{}}>{item.balance}</Text>
            </View>
            <View style={{ justifyContent: 'space-between', flex: 1, flexDirection: 'row', marginTop: 10 }}>
              <Text style={{}}>withdraw</Text>
              <Text style={{}}>{item.withdraw}</Text>
            </View>
          </View>
          <View
  style={{
    marginTop:10,
    borderBottomColor: '#c3c4c5',
    borderBottomWidth: 1,
  }}
/>
<View style={{ justifyContent: 'space-between', flex: 1, flexDirection: 'row' }}>
              <Text style={{}}>Current Balance</Text>
              <Text style={{}}>{item.currentBalance}</Text>
            </View>
        </View>
      </Card>
    )

  }

  return (
    <View style={Styles.container}>
      <FlatList
        data={CrediteArray}
        renderItem={({ item }) => <RowCredit item={item} />}
        keyExtractor={item => item.id}
      />


    </View>
  );
};

const Styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Colors.BACKGROUND_1,
    padding:2
  },
  titleText: {
    fontSize: 30,
    fontWeight: 'bold',
    alignSelf: 'center',
    marginTop: 15,
  },
  innerContainer: {
    paddingHorizontal: 15,
    justifyContent: 'center',
    alignItems: 'center',
    flex: 1,
  },
  inputFields: {
    width: '100%',
  },
  inputView: {
    marginBottom: 20,
  },
  inputLabelText: {
    fontSize: 20,
  },
  inputStyle: {
    backgroundColor: Colors.TEXT_INPUT,
    height: 60,
    marginTop: 5,
    paddingLeft: 15,
    justifyContent: 'center',
    textAlignVertical: 'center',
    fontSize: 19,
    borderWidth: 1.5,
    borderColor: '#dce9f6ff',
    //fontWeight: 'bold',
  },
  checkBoxContainerStyle: {
    backgroundColor: '#f2f7fcff',
    elevation: 0,
    borderWidth: 0,
    paddingLeft: 0,
    marginLeft: 0,
    marginTop: -10,
    marginBottom: 30,
  },
  checkBoxTextStyle: {
    fontSize: 19,
    fontWeight: 'normal',
  },
  buttonStyle: {
    width: '100%',
    backgroundColor: Colors.TOUCHABLE_BUTTON,
    height: 60,
    justifyContent: 'center',
    alignItems: 'center',
    //textAlignVertical: 'center',
  },
  loginText: {
    fontSize: 20,
    fontWeight: 'bold',
    letterSpacing: 1,
    color: '#ffffffff',
  },
  resetPasswordView: {
    marginTop: 50,
    justifyContent: 'center',
    alignItems: 'center',
  },
  resetPasswordTouchable: {
    paddingHorizontal: 8,
    paddingVertical: 3,
    borderRadius: 50,
  },
  resetPasswordText: {
    fontSize: 19,
  },
});

export default Credites;
