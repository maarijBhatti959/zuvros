import React from 'react';
import {View, TextInput, Text, StyleSheet} from 'react-native';
import {
  TouchableNativeFeedback,
  ScrollView,
} from 'react-native-gesture-handler';
import Colors from '../../../colors';

const AddGymLocation = ({navigation}) => {
  return (
    // <View style={Styles.container}>
   
      <ScrollView contentContainerStyle={Styles.innerContainer}>
      {/* <View style={Styles.progressIndicatorBackgroundView}>
        <View style={Styles.progressIndicator} />
      </View> */}
      <Text style={Styles.titleText}>Location Detail</Text>
      <Text style={Styles.descriptionText}>Enter gym details information</Text>
        <View style={Styles.inputFields}>
          <View style={Styles.inputView}>
            <Text style={Styles.inputLabelText}>City</Text>
            <TextInput placeholder={''} style={Styles.inputStyle} />
          </View>
          <View style={Styles.inputView}>
            <Text style={Styles.inputLabelText}>Zip Code</Text>
            <TextInput
              placeholder={'Zip Code'}
              style={Styles.inputStyle}
            />
          </View>
          <View style={Styles.inputView}>
            <Text style={Styles.inputLabelText}>Street Address</Text>
            <TextInput
              placeholder={''}
              secureTextEntry={true}
              style={Styles.inputStyle}
            />
          </View>
          <View style={Styles.inputView}>
            <Text style={Styles.inputLabelText}>Phone Number</Text>
            <TextInput
              placeholder={'+1-123456789'}
              secureTextEntry={true}
              style={Styles.inputStyle}
            />
          </View>

          {/* <View style={Styles.spacer} /> */}

          <TouchableNativeFeedback
            onPress={() => navigation.navigate('AdminGymFeatures')}
            style={Styles.buttonStyle}>
            <Text style={Styles.buttonText}>NEXT</Text>
          </TouchableNativeFeedback>
        </View>
      </ScrollView>
    // </View>
  );
};

const Styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Colors.BACKGROUND_1,
  },
  titleText: {
    fontSize: 26,
    fontWeight: 'bold',
    alignSelf: 'center',
  },
  descriptionText: {
    textAlign: 'center',
    fontSize: 19,
    marginHorizontal: 30,
    marginTop: 0,
    lineHeight: 30,
    color: '#64676bff',
  },
  innerContainer: {
    paddingHorizontal: 15,
    justifyContent: 'center',
    alignItems: 'center',
    paddingVertical: 5,
    backgroundColor: Colors.BACKGROUND_1,
    //flex: 1,
  },
  inputFields: {
    width: '100%',
  },
  inputView: {
    marginBottom: 20,
  },
  inputLabelText: {
    fontSize: 20,
  },
  inputStyle: {
    backgroundColor: Colors.TEXT_INPUT,
    marginTop: 3,
    paddingLeft: 15,
    justifyContent: 'center',
    textAlignVertical: 'center',
    fontSize: 15,
    elevation: 7,
  },
  spacer: {
    paddingVertical: 20,
  },
  buttonStyle: {
    width: '100%',
    backgroundColor: Colors.TOUCHABLE_BUTTON,
    height: 60,
    justifyContent: 'center',
    alignItems: 'center',
  },
  buttonText: {
    fontSize: 20,
    fontWeight: 'bold',
    letterSpacing: 1,
    color: '#ffffffff',
  },
  progressIndicatorBackgroundView: {
    backgroundColor: '#f1f1f1ff',
    width: '100%',
  },
  progressIndicator: {
    backgroundColor: '#226ddcff',
    height: 9,
    width: '33%',
  },
});

export default AddGymLocation;
