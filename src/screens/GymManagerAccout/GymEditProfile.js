import React, { useEffect, useRef, useState } from 'react';
import { View, Text, Dimensions, } from 'react-native';
import Colors from '../../../colors';
import { Icon, Divider, SearchBar, Button } from 'react-native-elements';
import { StyleSheet } from 'react-native';
import { TouchableNativeFeedback, FlatList, TouchableOpacity } from 'react-native-gesture-handler';
import { ScrollView } from 'react-native';
import Menu, { MenuItem } from 'react-native-material-menu';
import { StackedBarChart } from 'react-native-chart-kit';
import { GymEditProfileArray } from '../../../assets/Constant/Feed';
import RowGymEditProfile from '../../components/FlatListRows/RowGymEditProfile';


const initialLayout = { width: Dimensions.get('window').width };

const GymEditProfile = ({ navigation }) => {
    const [searchValue, setSearchValue] = useState('');

    const { width } = Dimensions.get('screen');

    const [index, setIndex] = React.useState(0);

    useEffect(() => {
        // navigation.setOptions({
        //     title: '',
        //     headerStyle: {
        //       backgroundColor: Colors.HEADER_BACKGROUND,
        //     },
        //     headerShown: true,
        //     headerLeft: () => (
        //       <View style={Styles.headerLeftView}>
        //        <View style={{height:40,width:40,borderRadius:20,backgroundColor:"#c4c4c4",}}/> 
        //         <Text style={Styles.headerLeftText}>Hunter Ballew</Text>
        //       </View>
        //     ),
        //     headerRight: () => (
        //       <View style={Styles.headerRightView}>
        //         <TouchableNativeFeedback
        //           onPress={() => navigation.navigate('AdminNotifications')}
        //           style={Styles.headerRightIconView}>
        //           <Icon
        //             name={'notifications-none'}
        //             type={'material'}
        //             size={25}
        //             color={'#ffffffff'}
        //             style={Styles.headerRightIcon}
        //           />
        //         </TouchableNativeFeedback>
        //         <TouchableNativeFeedback
        //           onPress={() => navigation.navigate('AdminInbox')}
        //           style={Styles.headerRightIconView}>
        //           <Icon
        //             name={'envelope'}
        //             type={'evilicon'}
        //             size={25}
        //             color={'#ffffffff'}
        //             style={Styles.headerRightIcon}
        //           />
        //         </TouchableNativeFeedback>
        //         <TouchableNativeFeedback onPress={() => navigation.toggleDrawer()}>
        //           <Icon
        //             name={'dots-vertical'}
        //             type={'material-community'}
        //             size={25}
        //             color={'#ffffffff'}
        //             style={Styles.headerRightIcon}
        //           />
        //         </TouchableNativeFeedback>
        //       </View>
        //     ),
        //   });
    });

    return (
        <ScrollView style={Styles.container}>
            <View style={{ flex: 1 }}>
                <View style={{ marginTop: 15, flexDirection: 'row', flex: 1, alignItems: 'center', justifyContent: 'space-between' }}>
                    <View style={{ height: 50, width: 50, borderRadius: 25, backgroundColor: 'gray' }} />
                    <TouchableOpacity onPress={()=>{navigation.navigate("GymDetail")}}>
                    <View style={{ flexDirection: 'column', alignItems: 'center' }}>
                        <Text style={Styles.socialIndicationTest}>23</Text>
                        <Text >Post</Text>
                    </View>
                    
                    </TouchableOpacity>
                    <TouchableOpacity onPress={()=>{navigation.navigate("GymDetail")}}>

                    <View style={{ flexDirection: 'column', alignItems: 'center' }}>
                        <Text style={Styles.socialIndicationTest}>35</Text>
                        <Text>Followers</Text>
                    </View>
                    </TouchableOpacity>
                    <TouchableOpacity onPress={()=>{navigation.navigate("GymDetail")}}>

                    <View style={{ flexDirection: 'column', alignItems: 'center' }}>
                        <Text style={Styles.socialIndicationTest}>43</Text>
                        <Text>Following</Text>
                    </View>
                    </TouchableOpacity>

                </View>
                <View style={{ marginTop: 30 }}>
                    <Text style={Styles.socialIndicationTest}>24 Hour Fitness</Text>
                    <Text>$50</Text>
                    <Text>Lorem ipsum dolor sit amet, consecteture adipiscing elit. Sed et elit tincidnt</Text>
                    <Button buttonStyle={{ height: 50, marginTop: 30 }} title="EDIT MY PROFILE" />

                </View>
                <View style={{ justifyContent: 'space-between', flexDirection: 'row', marginTop: 15 }}>
                    <Text>Locations</Text>
                    <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                        <Text>Add New</Text>
                        <TouchableOpacity onPress={()=>navigation.navigate('AddGymLocation')}>
                        <Icon
                            name={'add-circle-outline'}
                            type={'material'}
                            size={25}
                            color={'black'}
                            style={Styles.iconView}
                        />
                        </TouchableOpacity>
                        
                    </View>

                </View>
                <View
                    style={{
                        marginTop: 5,
                        borderBottomColor: '#c3c3c3',
                        borderBottomWidth: 1,
                    }}
                />

                <FlatList
                    data={GymEditProfileArray}
                    renderItem={({ item }) => <RowGymEditProfile item={item} />}
                    keyExtractor={item => item.id}
                />

            </View>

        </ScrollView>
    );
};

const Styles = StyleSheet.create({
    container: {
        width: '100%',
        flex: 1,
        paddingHorizontal: 15,
        backgroundColor: Colors.COLOR_WHITE,
    },
    socialIndicationTest: {
        fontWeight: 'bold', fontSize: 20
    },
    iconView: {
        paddingHorizontal: 6,
        borderRadius: 50,
    },

});

export default GymEditProfile;
