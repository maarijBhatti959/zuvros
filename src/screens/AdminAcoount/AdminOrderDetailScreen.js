import React, {useRef} from 'react';
import {View, Image, StyleSheet} from 'react-native';
import {Text, Icon, Divider} from 'react-native-elements';
import {ScrollView} from 'react-native-gesture-handler';
import Menu, {MenuItem} from 'react-native-material-menu';
import Colors from '../../../colors';

const AdminOrderDetail = () => {
  const menuRef = useRef([]);

  const hideMenu = i => {
    menuRef.current[i].hide();
  };

  const showMenu = i => {
    menuRef.current[i].show();
  };

  return (
    <ScrollView contentContainerStyle={Styles.container}>
      <View style={Styles.contentView}>
        <View style={Styles.statusView}>
          <Menu
            ref={r => (menuRef.current[0] = r)}
            button={
              <Icon
                name={'dots-vertical'}
                type={'material-community'}
                size={25}
                style={Styles.listItemMenuIcon}
                onPress={() => showMenu(0)}
              />
            }
            style={Styles.menuStyle}>
            <MenuItem
              onPress={() => hideMenu(0)}
              style={Styles.menuItemStyle}
              textStyle={Styles.menuTextStyle}>
              Completed
            </MenuItem>
            <Divider style={Styles.menuDividerStyle} />
            <MenuItem
              onPress={() => hideMenu(0)}
              style={Styles.menuItemStyle}
              textStyle={Styles.menuTextStyle}>
              Cancel
            </MenuItem>
          </Menu>
        </View>
        <View style={Styles.contentMainView}>
          <View style={Styles.contentMainViewImageView}>
            <Image
              source={require('../../../assets/images/Slice_3.png')}
              style={Styles.imageStyle}
            />
          </View>
          <View style={Styles.contentMainViewTextView}>
            <Text style={Styles.nameText}>24 HOUR FITNESS</Text>
            <Text style={Styles.costText}>$50.00 USD</Text>
            <Text style={Styles.viewDetailText}>View Details</Text>
          </View>
        </View>
        <Divider style={Styles.Divider} />
        <View style={Styles.detailView}>
          <Text style={Styles.nameText}>Shipping Address</Text>
          <Text style={Styles.descriptionText}>
            Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
            eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim
            ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut
            aliquip ex ea commodo consequat.
          </Text>
        </View>
        <Divider style={Styles.Divider} />
        <View style={Styles.contentViewList}>
          <View style={Styles.listItem}>
            <Text style={Styles.textStyle1}>Balance</Text>
            <Text style={Styles.textStyle1}>$300.00</Text>
          </View>
          <View style={Styles.spacer} />
          <View style={Styles.listItem}>
            <Text style={Styles.textStyle1}>Withdraw</Text>
            <Text style={Styles.textStyle1}>$300.00</Text>
          </View>
        </View>
        <Divider style={Styles.divider2} />
        <View style={Styles.spacer2} />
        <View style={Styles.listItem}>
          <Text style={Styles.textStyle2}>Balance</Text>
          <Text style={Styles.textStyle2}>$300.00</Text>
        </View>
      </View>

      <View style={Styles.spacer2} />
    </ScrollView>
  );
};

const Styles = StyleSheet.create({
  container: {
    flex: 1,
    width: '100%',
    backgroundColor: Colors.BACKGROUND_1,
    paddingHorizontal: 15,
    paddingVertical: 15,
  },
  contentView: {
    width: '100%',
    paddingHorizontal: 15,
    paddingVertical: 20,
    backgroundColor: '#ffffffff',
    borderRadius: 5,
    elevation: 5,
  },
  statusView: {
    flexDirection: 'row',
    justifyContent: 'flex-end',
    alignItems: 'center',
  },
  idText: {
    fontSize: 17,
    fontWeight: 'bold',
  },
  statusText: {
    fontSize: 12,
  },
  contentMainView: {
    flexDirection: 'row',
    marginTop: 0,
    alignItems: 'center',
    marginBottom: 20,
  },
  contentMainViewImageView: {
    width: 75,
    height: 90,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#ffffffff',
    borderWidth: 0.3,
  },
  imageStyle: {
    width: '60%',
    height: '60%',
  },
  contentMainViewTextView: {
    paddingLeft: 15,
  },
  nameText: {
    fontSize: 16.5,
    fontWeight: 'bold',
  },
  costText: {
    fontSize: 14,
    //fontWeight: 'bold',
  },
  viewDetailText: {
    fontSize: 15.5,
    marginTop: 15,
    //fontWeight: 'bold',
  },
  divider: {
    height: 1,
    backgroundColor: '#dadadaff',
    marginTop: 2,
  },
  contentViewList: {
    paddingVertical: 15,
  },
  listItem: {
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  textStyle1: {
    fontSize: 17,
  },
  textStyle2: {
    fontSize: 17,
    fontWeight: 'bold',
  },
  spacer: {
    height: 30,
  },
  spacer2: {
    height: 15,
  },
  divider2: {
    height: 2,
    backgroundColor: '#dadadaff',
  },
  menuStyle: {
    width: 200,
    height: 160,
    //marginRight: 15,
    paddingVertical: 20,
    borderRadius: 10,
  },
  menuItemStyle: {
    paddingVertical: 10,
  },
  menuTextStyle: {
    fontSize: 18,
  },
  menuDividerStyle: {marginHorizontal: 14},
  detailView: {
    paddingVertical: 20,
  },
  descriptionText: {
    marginTop: 15,
  },
});

export default AdminOrderDetail;
