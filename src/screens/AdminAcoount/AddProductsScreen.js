import React, {useState} from 'react';
import {View, TextInput, Text, StyleSheet} from 'react-native';
import Colors from '../../../colors';
import {CheckBox, Icon} from 'react-native-elements';
import {
  TouchableNativeFeedback,
  ScrollView,
} from 'react-native-gesture-handler';
import {RadioButton} from 'react-native-paper';

const AddProduct = ({navigation}) => {
  const [userType, setUserType] = useState('');

  return (
    <View style={Styles.container}>
      <ScrollView contentContainerStyle={Styles.innerContainer}>
        <View style={Styles.inputFields}>
          <View style={Styles.inputView2}>
            <Text style={Styles.inputLabelText}>Product Type</Text>
            <View style={{height: 15}} />
            <RadioButton.Group
              onValueChange={value => setUserType(value)}
              value={userType}>
              <View
                style={{
                  flexDirection: 'row',
                  alignItems: 'center',
                }}>
                <View style={Styles.radioView}>
                  <RadioButton value="Suppliments" />
                  <Text>Suppliments</Text>
                </View>
                <View style={{width: 15}} />
                <View style={Styles.radioView}>
                  <RadioButton value="Accessories" />
                  <Text>Fitness Accessories</Text>
                </View>
              </View>
            </RadioButton.Group>
          </View>

          <View style={Styles.inputView}>
            <Text style={Styles.inputLabelText}>Product Name</Text>
            <TextInput
              placeholder={''}
              textContentType={'name'}
              keyboardType={'default'}
              style={Styles.inputStyle}
            />
          </View>
          <View style={Styles.inputView}>
            <Text style={Styles.inputLabelText}>Product Description</Text>
            <TextInput
              placeholder={''}
              keyboardType={'default'}
              style={Styles.inputStyle}
            />
          </View>
          <View style={Styles.inputView}>
            <Text style={Styles.inputLabelText}>Price</Text>
            <TextInput
              placeholder={'$45.00'}
              keyboardType={'default'}
              style={Styles.inputStyle}
            />
          </View>

          <View style={Styles.inputView}>
            <TouchableNativeFeedback
              style={Styles.deviceuploadBtn}
              onPress={() => null}>
              <View style={{flexDirection: 'row', alignItems: 'center'}}>
                <Icon name={'device-mobile'} type={'octicon'} size={20} />
                <Text>{'    '}</Text>
                <Text style={Styles.deviceuploadBtnText}>
                  Upload images from your device
                </Text>
              </View>
            </TouchableNativeFeedback>
          </View>

          <TouchableNativeFeedback
            style={Styles.buttonStyle}
            onPress={() => null}>
            <Text style={Styles.loginText}>ADD PRODUCT</Text>
          </TouchableNativeFeedback>
        </View>
      </ScrollView>
    </View>
  );
};

const Styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Colors.BACKGROUND_1,
  },
  titleText: {
    fontSize: 30,
    fontWeight: 'bold',
    alignSelf: 'center',
    marginTop: 15,
  },
  innerContainer: {
    paddingHorizontal: 15,
    paddingTop: 20,
    //justifyContent: 'flex-end',
    alignItems: 'center',
    //flex: 1,
  },
  inputFields: {
    width: '100%',
    //bottom: 30,
  },
  inputView: {
    marginBottom: 20,
  },
  inputView2: {
    marginBottom: 20,
  },
  inputLabelText: {
    fontSize: 16,
  },
  inputStyle: {
    backgroundColor: Colors.TEXT_INPUT,
    height: 50,
    marginTop: 5,
    paddingLeft: 15,
    justifyContent: 'center',
    textAlignVertical: 'center',
    fontSize: 16,
    borderWidth: 1.5,
    borderColor: '#dce9f6ff',
    //fontWeight: 'bold',
  },
  checkBoxContainerStyle: {
    backgroundColor: '#f2f7fcff',
    elevation: 0,
    borderWidth: 0,
    paddingLeft: 0,
    marginLeft: 0,
    marginTop: -10,
    marginBottom: 30,
  },
  checkBoxTextStyle: {
    fontSize: 19,
    fontWeight: 'normal',
  },
  buttonStyle: {
    width: '100%',
    backgroundColor: Colors.TOUCHABLE_BUTTON,
    height: 55,
    justifyContent: 'center',
    alignItems: 'center',
    //textAlignVertical: 'center',
  },
  loginText: {
    fontSize: 18,
    fontWeight: 'bold',
    letterSpacing: 1,
    color: '#ffffffff',
  },
  resetPasswordView: {
    marginTop: 50,
    justifyContent: 'center',
    alignItems: 'center',
  },
  resetPasswordTouchable: {
    paddingHorizontal: 8,
    paddingVertical: 3,
    borderRadius: 50,
  },
  resetPasswordText: {
    fontSize: 19,
  },
  radioView: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  deviceuploadBtn: {
    //flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    height: 50,
    backgroundColor: '#dee2e7ff',
  },
  deviceuploadBtnText: {
    fontSize: 18,
  },
});

export default AddProduct;
