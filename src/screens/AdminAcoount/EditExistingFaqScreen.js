import React, {useState, useRef} from 'react';
import {View, TextInput, Text, StyleSheet, Image} from 'react-native';
import Colors from '../../../colors';
import {
  TouchableNativeFeedback,
  ScrollView,
} from 'react-native-gesture-handler';
import {RadioButton} from 'react-native-paper';
import Menu, {MenuItem} from 'react-native-material-menu';
import {Icon, Divider, Card} from 'react-native-elements';

const EditExistingFaq = ({navigation}) => {
  const [userType, setUserType] = useState('');
  const menuRef = useRef([]);

  const hideMenu = i => {
    menuRef.current[i].hide();
  };

  const showMenu = i => {
    menuRef.current[i].show();
  };

  return (
    <View style={Styles.container}>
      <ScrollView contentContainerStyle={Styles.innerContainer}>
        <View style={Styles.inputFields}>
          <View style={Styles.inputView}>
            <Text style={Styles.inputLabelText}>All questions</Text>
            <View
              style={{
                backgroundColor: Colors.TEXT_INPUT,
                marginTop: 15,
                padding: 15,
                elevation: 5,
              }}>
              <View
                style={{
                  flexDirection: 'row',
                  justifyContent: 'space-between',
                  alignItems: 'center',
                }}>
                <Text style={{fontWeight: 'bold', fontSize: 14}}>
                  Question name
                </Text>
                <Menu
                  ref={r => (menuRef.current[0] = r)}
                  button={
                    <Icon
                      name={'dots-vertical'}
                      type={'material-community'}
                      size={25}
                      style={Styles.listItemMenuIcon}
                      onPress={() => showMenu(0)}
                    />
                  }
                  style={Styles.menuStyle}>
                  <MenuItem
                    onPress={() => hideMenu(0)}
                    style={Styles.menuItemStyle}
                    textStyle={Styles.menuTextStyle}>
                    Edit
                  </MenuItem>
                  <Divider style={{marginHorizontal: 5}} />
                  <MenuItem
                    onPress={() => hideMenu(0)}
                    style={Styles.menuItemStyle}
                    textStyle={Styles.menuTextStyle}>
                    Delete
                  </MenuItem>
                </Menu>
              </View>
              <Text style={{marginTop: 13}}>
                Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla
                laoreet vel massa id ullamcorper. In efficitur odio vel erat
                dignissim pretium. Fusce tristique sed felis vitae fermentum.
              </Text>
            </View>
          </View>
          <View style={Styles.inputView}>
            <Text style={Styles.inputLabelText}>All Videos</Text>
            <ScrollView
              horizontal
              contentContainerStyle={{marginTop: 15, flexDirection: 'row'}}>
              <Card containerStyle={{padding: 0, margin: 0}}>
                <View style={Styles.user}>
                  <Image
                    style={{width: 150, height: 150}}
                    resizeMode="cover"
                    source={require('../../../assets/images/sample-profile-img.jpg')}
                  />
                  <View
                    style={{
                      flexDirection: 'row',
                      justifyContent: 'space-between',
                      alignItems: 'center',
                      paddingVertical: 10,
                    }}>
                    <Text
                      style={{fontWeight: 'bold', fontSize: 14, marginLeft: 5}}>
                      Health and Fitness
                    </Text>
                    <Menu
                      ref={r => (menuRef.current[1] = r)}
                      button={
                        <Icon
                          name={'dots-vertical'}
                          type={'material-community'}
                          size={15}
                          // style={{}}
                          onPress={() => showMenu(1)}
                        />
                      }
                      style={Styles.menuStyle}>
                      <MenuItem
                        onPress={() => hideMenu(1)}
                        style={Styles.menuItemStyle}
                        textStyle={Styles.menuTextStyle}>
                        Edit
                      </MenuItem>
                      <Divider style={{marginHorizontal: 5}} />
                      <MenuItem
                        onPress={() => hideMenu(1)}
                        style={Styles.menuItemStyle}
                        textStyle={Styles.menuTextStyle}>
                        Delete
                      </MenuItem>
                    </Menu>
                  </View>
                </View>
              </Card>
              <View style={{width: 20}} />
              <Card containerStyle={{padding: 0, margin: 0}}>
                <View style={Styles.user}>
                  <Image
                    style={{width: 150, height: 150}}
                    resizeMode="cover"
                    source={require('../../../assets/images/sample-profile-img.jpg')}
                  />
                  <View
                    style={{
                      flexDirection: 'row',
                      justifyContent: 'space-between',
                      alignItems: 'center',
                      paddingVertical: 10,
                    }}>
                    <Text
                      style={{fontWeight: 'bold', fontSize: 14, marginLeft: 5}}>
                      Health and Fitness
                    </Text>
                    <Menu
                      ref={r => (menuRef.current[2] = r)}
                      button={
                        <Icon
                          name={'dots-vertical'}
                          type={'material-community'}
                          size={15}
                          // style={{}}
                          onPress={() => showMenu(2)}
                        />
                      }
                      style={Styles.menuStyle}>
                      <MenuItem
                        onPress={() => hideMenu(2)}
                        style={Styles.menuItemStyle}
                        textStyle={Styles.menuTextStyle}>
                        Edit
                      </MenuItem>
                      <Divider style={{marginHorizontal: 5}} />
                      <MenuItem
                        onPress={() => hideMenu(2)}
                        style={Styles.menuItemStyle}
                        textStyle={Styles.menuTextStyle}>
                        Delete
                      </MenuItem>
                    </Menu>
                  </View>
                </View>
              </Card>
              <View style={{width: 20}} />
            </ScrollView>
          </View>
        </View>
      </ScrollView>
    </View>
  );
};

const Styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Colors.BACKGROUND_1,
  },
  titleText: {
    fontSize: 30,
    fontWeight: 'bold',
    alignSelf: 'center',
    marginTop: 15,
  },
  innerContainer: {
    paddingHorizontal: 15,
    paddingTop: 20,
    //justifyContent: 'flex-end',
    alignItems: 'center',
    //flex: 1,
  },
  inputFields: {
    width: '100%',
    //bottom: 30,
  },
  inputView: {
    marginBottom: 20,
  },
  inputView2: {
    marginBottom: 20,
  },
  inputLabelText: {
    fontSize: 16,
  },
  inputStyle: {
    backgroundColor: Colors.TEXT_INPUT,
    height: 50,
    marginTop: 5,
    paddingLeft: 15,
    justifyContent: 'center',
    textAlignVertical: 'center',
    fontSize: 16,
    borderWidth: 1.5,
    borderColor: '#dce9f6ff',
    //fontWeight: 'bold',
  },
  checkBoxContainerStyle: {
    backgroundColor: '#f2f7fcff',
    elevation: 0,
    borderWidth: 0,
    paddingLeft: 0,
    marginLeft: 0,
    marginTop: -10,
    marginBottom: 30,
  },
  checkBoxTextStyle: {
    fontSize: 19,
    fontWeight: 'normal',
  },
  buttonStyle: {
    width: '100%',
    backgroundColor: Colors.TOUCHABLE_BUTTON,
    height: 55,
    justifyContent: 'center',
    alignItems: 'center',
    //textAlignVertical: 'center',
  },
  loginText: {
    fontSize: 18,
    fontWeight: 'bold',
    letterSpacing: 1,
    color: '#ffffffff',
  },
  resetPasswordView: {
    marginTop: 50,
    justifyContent: 'center',
    alignItems: 'center',
  },
  resetPasswordTouchable: {
    paddingHorizontal: 8,
    paddingVertical: 3,
    borderRadius: 50,
  },
  resetPasswordText: {
    fontSize: 19,
  },
  radioView: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  deviceuploadBtn: {
    //flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    height: 50,
    backgroundColor: '#dee2e7ff',
  },
  deviceuploadBtnText: {
    fontSize: 18,
  },
});

export default EditExistingFaq;
