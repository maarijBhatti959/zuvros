import React from 'react';
import {View, StyleSheet} from 'react-native';
import {Text, Divider} from 'react-native-elements';
import {ScrollView} from 'react-native-gesture-handler';
import Colors from '../../../colors';

const AdminTransactionCredited = () => {
  return (
    <ScrollView contentContainerStyle={Styles.container}>
      <View style={Styles.contentView}>
        <View style={Styles.idDateView}>
          <Text style={Styles.idText}>ID: #12456789</Text>
          <Text style={Styles.dateText}>23 Dec 2019 07:14</Text>
        </View>
        <View style={Styles.contentMainView}>
          <View style={Styles.contentMainViewTextView}>
            <Text style={Styles.textStyleH}>From Bank Account</Text>
          </View>
        </View>
        <Divider style={Styles.divider2} />
        <View style={Styles.spacer2} />
        <View style={Styles.listItem}>
          <Text style={Styles.textStyle2}>Total Debited</Text>
          <Text style={Styles.textStyle2}>$300.00</Text>
        </View>
      </View>

      <View style={Styles.spacer2} />

      <View style={Styles.contentView}>
        <View style={Styles.idDateView}>
          <Text style={Styles.idText}>ID: #12456789</Text>
          <Text style={Styles.dateText}>23 Dec 2019 07:14</Text>
        </View>
        <View style={Styles.contentMainView}>
          <View style={Styles.contentMainViewTextView}>
            <Text style={Styles.textStyleH}>From Bank Account</Text>
          </View>
        </View>
        <Divider style={Styles.divider2} />
        <View style={Styles.spacer2} />
        <View style={Styles.listItem}>
          <Text style={Styles.textStyle2}>Total Debited</Text>
          <Text style={Styles.textStyle2}>$300.00</Text>
        </View>
      </View>

      <View style={Styles.spacer2} />

      <View style={Styles.contentView}>
        <View style={Styles.idDateView}>
          <Text style={Styles.idText}>ID: #12456789</Text>
          <Text style={Styles.dateText}>23 Dec 2019 07:14</Text>
        </View>
        <View style={Styles.contentMainView}>
          <View style={Styles.contentMainViewTextView}>
            <Text style={Styles.textStyleH}>From Bank Account</Text>
          </View>
        </View>
        <Divider style={Styles.divider2} />
        <View style={Styles.spacer2} />
        <View style={Styles.listItem}>
          <Text style={Styles.textStyle2}>Total Debited</Text>
          <Text style={Styles.textStyle2}>$300.00</Text>
        </View>
      </View>

      <View style={Styles.spacer2} />
    </ScrollView>
  );
};

const Styles = StyleSheet.create({
  container: {
    //flex: 1,
    width: '100%',
    backgroundColor: Colors.BACKGROUND_1,
    paddingHorizontal: 15,
    paddingVertical: 15,
  },
  contentView: {
    width: '100%',
    paddingHorizontal: 15,
    paddingVertical: 20,
    backgroundColor: '#ffffffff',
    borderRadius: 5,
    elevation: 5,
  },
  idDateView: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  idText: {
    fontSize: 17,
    fontWeight: 'bold',
  },
  dateText: {
    fontSize: 14,
  },
  contentMainView: {
    flexDirection: 'row',
    marginTop: 10,
    alignItems: 'center',
    marginBottom: 30,
  },
  contentMainViewImageView: {
    width: 50,
    height: 50,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 50 / 2,
    borderWidth: 0.5,
  },
  imageStyle: {
    width: '60%',
    height: '60%',
  },
  nameText: {
    fontSize: 16.5,
    fontWeight: 'bold',
  },
  costText: {
    fontSize: 16,
    fontWeight: 'bold',
  },
  divider: {
    height: 1,
    backgroundColor: '#dadadaff',
  },
  contentViewList: {
    paddingVertical: 15,
  },
  listItem: {
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  textStyle1: {
    fontSize: 17,
  },
  textStyle2: {
    fontSize: 17,
    fontWeight: 'bold',
  },
  textStyleH: {
    fontSize: 20,
    fontWeight: 'bold',
  },
  spacer: {
    height: 30,
  },
  spacer2: {
    height: 15,
  },
  divider2: {
    height: 2,
    backgroundColor: '#dadadaff',
  },
});

export default AdminTransactionCredited;
