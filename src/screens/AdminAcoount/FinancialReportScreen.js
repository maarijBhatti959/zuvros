import React from 'react';
import {View, Text} from 'react-native';
import Colors from '../../../colors';
import {StyleSheet} from 'react-native';
import {ScrollView} from 'react-native';
import {PieChart} from 'react-native-svg-charts';

const FinancialReport = ({navigation}) => {
  const data = [50, 10, 40, 95, -4, -24, 85, 91, 35, 53, -53, 24, 50, -20, -80];
  const randomColor = () =>
    ('#' + ((Math.random() * 0xffffff) << 0).toString(16) + '000000').slice(
      0,
      7,
    );

  const pieData = data
    .filter(value => value > 0)
    .map((value, index) => ({
      value,
      svg: {
        fill: randomColor(),
        onPress: () => console.log('press', index),
      },
      key: `pie-${index}`,
    }));
  return (
    <ScrollView
      contentContainerStyle={Styles.container}
      showsVerticalScrollIndicator={false}>
      <View style={Styles.innerContainer}>
        <Text style={Styles.headerText}>Annual Financial Report</Text>
        <View>
          <PieChart style={{height: 200}} data={pieData} />
        </View>
      </View>
    </ScrollView>
  );
};

const Styles = StyleSheet.create({
  headerLeftView: {
    paddingLeft: 8,
  },
  headerLeftText: {
    fontSize: 20,
    fontWeight: 'bold',
  },
  headerRightView: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'flex-end',
    margin: 0,
    paddingRight: 8,
  },
  headerRightIconView: {
    paddingHorizontal: 6,
    paddingVertical: 6,
    borderRadius: 50,
  },
  headerRightIcon: {
    borderRadius: 50,
  },
  container: {
    width: '100%',
    flex: 1,
    backgroundColor: Colors.COLOR_WHITE,
  },
  innerContainer: {
    width: '100%',
    paddingHorizontal: 8,
    paddingVertical: 25,
    backgroundColor: Colors.COLOR_WHITE,
  },
  headerText: {
    fontSize: 23,
    //fontWeight: 'bold',
  },
});

export default FinancialReport;
