import React from 'react';
import {createMaterialTopTabNavigator} from '@react-navigation/material-top-tabs';
import nullScreen from '../screens/AdminAcoount/NullScreen';
import AdminAllOrdersList from '../screens/AdminAcoount/AdminAllOrdersListScreen';

const Tab = createMaterialTopTabNavigator();

const AdminOrdersTabs = () => {
  return (
    <Tab.Navigator
      tabBarOptions={{
        labelStyle: {
          fontSize: 18,
          textTransform: 'none',
          color: '#ffffffff',
        },
        indicatorStyle: {
          backgroundColor: '#ffffffff',
        },
        style: {
          backgroundColor: '#46BAFF',
        },
      }}>
      <Tab.Screen
        name="AdminAllOrdersList"
        component={AdminAllOrdersList}
        options={{
          title: 'All Orders',
        }}
      />
      <Tab.Screen
        name="Completed"
        component={nullScreen}
        options={{
          title: 'Credited',
        }}
      />
      <Tab.Screen
        name="NewOrders"
        component={nullScreen}
        options={{
          title: 'New Orders',
        }}
      />
    </Tab.Navigator>
  );
};

export default AdminOrdersTabs;
