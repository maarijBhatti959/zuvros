import React, { useEffect, useRef, useState } from 'react';
import { View, Text, Dimensions, StyleSheet, Image, TouchableOpacity } from 'react-native';
import { Button } from 'react-native-elements';
import { Icon, Divider, SearchBar } from 'react-native-elements';
import { TouchableNativeFeedback } from 'react-native-gesture-handler';



export default function GymHomeRow({ item }) {
    return (
        <View style={styles.item}>
            <View style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between' }}>
                <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                    <View style={{ height: 50, width: 50, borderRadius: 25, backgroundColor: "#c4c4c470" }} />
                    <View style={{ flexDirection: 'column' ,marginLeft:10}}>
                        <Text>{item.name}</Text>
                        <View style={{flexDirection:'row'}}>
                        <Icon
                            name={'map-marker-outline'}
                            type={'material-community'}
                            size={25}
                            color={'black'}
                        />
                        <Text>{item.loc}</Text>
                        </View>
                    
                    </View>
                </View>
                <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                    <Button title='Follow' />
                    <TouchableNativeFeedback style={{ marginLeft: 5 }} onPress={() => alert("soon")}>
                        <Icon
                            name={'dots-vertical'}
                            type={'material-community'}
                            size={25}
                            color={'black'}
                        />
                    </TouchableNativeFeedback>
                </View>

            </View>
            <Image
                style={{ width: "100%", marginTop: 5 }}
                source={item.image}
            />
            <View style={{ marginTop: 5, flex: 1, flexDirection: 'row', justifyContent: 'space-between' }}>
                <View style={{ flexDirection: 'column', alignItems: 'center' }}>
                    <View style={{ flexDirection: 'row' }}>
                        <Icon
                            name={'like'}
                            type={'evilicon'}
                            size={25}
                            color={'black'}
                        />
                        <Text>likes</Text>
                    </View>
                    <Text>{item.likes}</Text>
                </View>
                <View
                    style={{
                        borderLeftWidth: 1,
                        borderLeftColor: '#c3c4c4',
                    }}
                />
                <View style={{ flexDirection: 'column', alignItems: 'center' }}>
                    <View style={{ flexDirection: 'row' }}>
                        <Icon
                            name={'comment'}
                            type={'evilicon'}
                            size={25}
                            color={'black'}
                        />
                        <Text>comments</Text>
                    </View>
                    <Text>{item.comments}</Text>
                </View>
                <View
                    style={{
                        borderLeftWidth: 1,
                        borderLeftColor: '#c3c4c4',
                    }}
                />
                <View style={{ flexDirection: 'column', alignItems: 'center' }}>
                    <View style={{ flexDirection: 'row' }}>
                        <Icon
                            name={'share-google'}
                            type={'evilicon'}
                            size={25}
                            color={'black'}
                        />
                        <Text>share</Text>
                    </View>
                    <Text>{item.share}</Text>
                </View>



            </View>
        </View>
    );
}
const styles = StyleSheet.create({
    container: {
        flex: 1,
        marginTop: 16,
    },
    item: {
        padding: 10,
        marginVertical: 8,
        marginHorizontal: 8,
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,

        elevation: 5,
    },
    title: {
        fontSize: 32,
    },
});