import React,{useReducer,createContext} from 'react';
import Routes from './src/navigation/Routes';
import { ThemeProvider } from './AppContext';


const App = () => {
  return (
    <ThemeProvider>
        <Routes/>
    </ThemeProvider>
    
  );
};

export default App;
